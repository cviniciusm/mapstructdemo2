package br.eti.cvm.mapstructdemo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mapstructdemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(Mapstructdemo2Application.class, args);
	}

}
