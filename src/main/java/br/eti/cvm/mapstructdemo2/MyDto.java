package br.eti.cvm.mapstructdemo2;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MyDto {

    private String name;

    private Date birthday;

    private Integer age;

}
