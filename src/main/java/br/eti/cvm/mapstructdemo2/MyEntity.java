package br.eti.cvm.mapstructdemo2;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MyEntity {

    private String name;

    private LocalDate birthday;

}
