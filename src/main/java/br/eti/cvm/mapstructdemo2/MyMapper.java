package br.eti.cvm.mapstructdemo2;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.time.LocalDate;
import java.time.Period;

@Mapper
public interface MyMapper {

    MyEntity toEntity(MyDto dto);

    MyDto toDto(MyEntity entity);

    @AfterMapping
    default void getAge(MyEntity entity, @MappingTarget MyDto dto) {
        Period age = Period.between(entity.getBirthday(), LocalDate.now());

        dto.setAge(age.getYears());
    }

}
